<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() ) : ?>
  <?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>


<div class="wrapper pt-0" id="full-width-page-wrapper">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php $hero_video = get_field('hero_video'); ?>

		<section class="wrapper bg-centered pb-5" id="hero" style="background-image:url(<?php echo get_field('placeholder_image')['sizes']['large']; ?>);">

			<div class="hero-cover"></div>

			<div class="container pb-5 pt-5 mb-5" id="hero-copy">

				<div class="col-md-9 pb-5 mb-5 hero-content">

					<h1><?php echo get_field('hero_copy'); ?></h1>

				</div>

			</div>

			<?php if ( $hero_video ) { ?>

				<div id="play-button-wrapper">

					<a class="play-video text-dark">

						<span class="play-arrow"></span>

						<span>Watch the video</span>

					</a>

				</div>

			<?php } ?>

		</section>

		<section class="container" id="about-container">

			<a name="about" class="anchor-tag"></a>

			<div class="row d-flex align-items-center">

				<div class="col-md-4">

					<h5 class="text-uppercase mb-5 font-weight-bold"><?php the_field('about_title'); ?></h5>

					<?php the_field('about_content'); ?>

				</div>

				<div class="col-md-8 about-us-image">

					<?php echo get_about_svg(); ?>

					<img class="crowdsourcing" alt="crowdsourcing" src="<?php echo get_field('about_image')['sizes']['large']; ?>"/>

				</div>

			</div>

		</section>

		<?php
		if ( $history = get_field('history_statistics') ) { ?>

			<section class="container" id="history-container">

				<div id="history-content">

					<div class="row d-flex align-items-center justify-content-between">

						<?php
						foreach ( $history as $item ) { ?>

							<div class="col-sm-auto">

								<div class="statistic-container">

									<div class="statistics-content">

										<div class="border-span-x"></div>

										<div class="border-span-y"></div>

										<div class="statistic-icon bg-centered" style="background-image:url(<?php echo $item['history_icon']['sizes']['small']; ?>);">

										</div>

										<div class="statistic-number" data-count="<?php echo $item['history_number']; ?>">

											<span>0</span>

										</div>

										<p class="statistic-description"><?php echo $item['statistic_description']; ?></p>

									</div>

								</div>

							</div>

							<?php
						} ?>

					</div>

				</div>

				<?php $label = get_field('history_module_label') ? get_field('history_module_label') : 'Project History'; ?>

				<div class="history-arrows">

					<div class="history-arrow history-arrow-left">

					</div>

					<span id="history-module-label"><?php echo $label; ?></span>

					<div class="history-arrow history-arrow-right">

					</div>

				</div>

			</section>

		<?php
		} ?>

		<section class="container">

			<div class="row">

				<h2 class="col-md-12 content-area mb-5">
					Available Products
				</h2>

				<?php $product_types = get_terms(['taxonomy' => 'product-type']); ?>

				<div class="col-md-12">

					<div class="row pt-5">

						<?php foreach( $product_types as $key=>$p_type) { ?>

							<?php $args = [
								'post_type' => 'product',
								'posts_per_page' => -1,
								'tax_query' => [
									[
										'taxonomy' => 'product-type',
										'field' => 'term_id',
										'terms' => [ $p_type->term_id ]
									]
								]
							];
							$product_query = new WP_Query( $args );
							if( $product_query->have_posts() ): ?>

								<div class="col-md-5 mb-5<?php echo $key % 2 != 0 ? ' offset-md-1' : ''; ?>">

									<h6 class="text-uppercase text-secondary font-weight-bold mb-5"><?php echo $p_type->name; ?></h6>

									<div class="list-group list-group-flush product-list">

										<?php while ( $product_query->have_posts() ) : $product_query->the_post(); ?>

												<a href="#<?php echo basename(get_permalink()); ?>" class="list-group-item list-group-item-action product-list-item px-0 d-flex justify-content-between align-items-center py-4">

													<span class="border-span"></span>

													<div class="d-flex align-items-center">

														<span class="product-icon d-inline-flex align-items-center mx-3">

															<img alt="<?php echo get_field('icon')['title']; ?>" src="<?php echo get_field('icon')['sizes']['small']; ?>" alt="<?php echo get_field('icon')['title']; ?>"/>

														</span>

														<span class="product-list-item-title"><?php the_title(); ?></span>

													</div>

													<span class="arrow mr-3"></span>

												</a>

												<span class="product-item-bottom-border"></span>

										<?php endwhile; wp_reset_query(); ?>

									</div>

								</div>

							<?php endif; ?>

						<?php } ?>

					</div><!-- .row end -->

				</div>

			</div>

		</section><!-- .container end -->

		<?php if ( $divider_copy = get_field('divider_copy') ) { ?>

			<section class="wrapper divider mb-5">

				<div class="container py-5">

					<h2 class="text-center text-white font-weight-bold"><?php echo $divider_copy; ?></h2>

				</div>

			</section>

		<?php } ?>

		<section class="wrapper">

			<div class="container">

				<?php foreach( $product_types as $key=>$p_type) { ?>

					<?php $args = [
						'post_type' => 'product',
						'posts_per_page' => -1,
						'tax_query' => [
							[
								'taxonomy' => 'product-type',
								'field' => 'term_id',
								'terms' => [ $p_type->term_id ]
							]
						]
					];
					$product_query = new WP_Query( $args );
					if( $product_query->have_posts() ): ?>

						<div id="products-info-wrapper">

							<a class="anchor-tag" name="<?php echo $p_type->slug; ?>"></a>

							<h3 class="mb-5 mt-3 d-inline-block"><?php echo $p_type->name; ?></h3>

							<div class="products-info my-5">

								<?php while ( $product_query->have_posts() ) : $product_query->the_post(); ?>

									<a name="<?php echo basename(get_permalink()); ?>"></a>

									<div class="row d-flex product-item mb-4 <?php echo $key % 2 != 0 ? 'flex-row-reverse justify-content-between' : 'flex-row'; ?>">

										<div class="col-md-6">

											<div class="product-image">

												<img alt="<?php echo get_post(get_post_thumbnail_id())->post_title; ?>" class="img-fluid" src="<?php echo get_the_post_thumbnail_url( get_the_id(), 'medium'); ?>"/>

											</div>

										</div>

										<div class="col-md-4 offset-md-1">

											<div class="product-slides">

												<div class="product-slide">

													<h4 class="my-5"><?php the_title(); ?></h4>

													<div class="product-content">

														<?php the_content(); ?>

														<?php if ( $ctas = get_field('ctas') ) { ?>

															<?php the_product_ctas($ctas); ?>

														<?php } ?>

													</div>

												</div>

												<?php if ( $slides = get_field('product_slides') ) { ?>

													<?php foreach ( $slides as $slide ) { ?>

														<div class="product-slide">

															<h4><?php echo $slide['title']; ?></h4>

															<div class="product-content">

																<?php echo $slide['content']; ?>

																<?php if ( $ctas = $slide['ctas'] ) { ?>

																	<?php the_product_ctas($ctas); ?>

																<?php } ?>

															</div>

														</div>

													<?php } ?>

												<?php } ?>

											</div>

										</div>

									</div>

								<?php endwhile; wp_reset_query(); ?>

							</div>

						</div>

					<?php endif; ?>

				<?php } ?>

			</div>

		</section>

	<?php endwhile; // end of the loop. ?>

</div><!-- #full-width-page-wrapper -->

<?php if ( $hero_video ) { ?>

	<div id="video-background-wrapper">

		<div id="video-background-container">

			<div id="video-background-close">

			</div>

			<video controls id="video-background">

				<source src="<?php echo $hero_video['url']; ?>" type="<?php echo $hero_video['type']; ?>/<?php echo $hero_video['subtype']; ?>">

			</video>

		</div>

	</div>

<?php } ?>

<?php get_footer(); ?>
