<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.typekit.net/rmx2iue.css">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:100,200" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-light py-0 px-4 pt-3">

		<?php if ( 'container' == $container ) : ?>
			<div class="container-fluid border-bottom border-muted">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				<div class="navbar-toggle">
					<span class="navbar-toggler-icon"></span>
				</div>

				<!-- The WordPress Menu goes here -->
				<?php if($nav_items = wp_get_nav_menu_items( 'main-menu' )) { ?>
					<div id="mobile-nav">
						<a id="navbar-close"><img src="<?php echo get_template_directory_uri(); ?>/images/x.png"/></a>
						<p id="navbar-label">Content</p>
						<ul id="main-menu" class="navbar-nav ml-auto">
							<?php foreach($nav_items as $nav_item) { ?>
								<li id="menu-item-<?php echo $nav_item->ID; ?>" itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement">
									<a href="<?php echo $nav_item->url; ?>"><?php echo $nav_item->title; ?></a>
								</li>
							<?php } ?>
						</ul>
						<?php if( $about_image = get_field('about_image', get_option('page_on_front')) ) { ?>
							<img src="<?php echo $about_image['sizes']['medium']; ?>"/>
						<?php } ?>
					</div>
				<?php } ?>


				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
