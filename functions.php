<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

function get_about_svg() {
	ob_start();
	global $is_IE;
	if (!$is_IE) { ?>
		<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 708.1 670.3" style="enable-background:new 0 0 708.1 670.3;" xml:space="preserve">
			<style type="text/css">
				.st0{opacity:0.25;fill:none;stroke:url(#SVGID_1_);stroke-miterlimit:10;}
				.st1{opacity:0.39;fill:none;stroke:url(#SVGID_2_);stroke-miterlimit:10;}
				.st2{opacity:0.78;fill:none;stroke:url(#SVGID_3_);stroke-miterlimit:10;}
				.st3{opacity:4.000000e-02;fill:none;stroke:url(#SVGID_4_);stroke-miterlimit:10;}
				.st4{opacity:0.37;fill:none;stroke:url(#SVGID_5_);stroke-miterlimit:10;}
				.st5{opacity:0.82;fill:none;stroke:url(#SVGID_6_);stroke-miterlimit:10;}
				.st6{opacity:0.17;fill:none;stroke:url(#SVGID_7_);stroke-miterlimit:10;}
				.st7{opacity:0.66;fill:none;stroke:url(#SVGID_8_);stroke-miterlimit:10;}
				.st8{opacity:0.21;fill:none;stroke:url(#SVGID_9_);stroke-miterlimit:10;}
			</style>
			<radialGradient id="SVGID_1_" cx="444.95" cy="356.3515" r="259.203" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st0" points="186.2,245.6 333.1,245.6 333.1,97.6 555.7,97.6 555.7,245.6 703.7,245.6 703.7,467.1 555.7,467.1
				555.7,615.1 333.1,615.1 333.1,468.2 186.2,468.2 "></polygon>
			<radialGradient id="SVGID_2_" cx="496.2407" cy="212.5947" r="213.0947" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st1" points="283.6,121.6 404.3,121.6 404.3,0 587.2,0 587.2,121.6 708.8,121.6 708.8,303.6 587.2,303.6
				587.2,425.2 404.3,425.2 404.3,304.5 283.6,304.5 "></polygon>
			<radialGradient id="SVGID_3_" cx="368.3176" cy="308.6063" r="244.3623" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st2" points="124.5,204.2 262.9,204.2 262.9,64.7 472.7,64.7 472.7,204.2 612.2,204.2 612.2,413 472.7,413
				472.7,552.5 262.9,552.5 262.9,414.1 124.5,414.1 "></polygon>
			<radialGradient id="SVGID_4_" cx="357.6705" cy="440.9842" r="229.7076" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st3" points="128.5,342.9 258.6,342.9 258.6,211.8 455.8,211.8 455.8,342.9 586.9,342.9 586.9,539.1 455.8,539.1
				455.8,670.2 258.6,670.2 258.6,540.1 128.5,540.1 "></polygon>
			<radialGradient id="SVGID_5_" cx="344.9654" cy="353.7099" r="199.2587" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st4" points="146.2,268.6 259,268.6 259,155 430,155 430,268.6 543.7,268.6 543.7,438.8 430,438.8 430,552.5
				259,552.5 259,439.7 146.2,439.7 "></polygon>
			<radialGradient id="SVGID_6_" cx="422.7629" cy="204.1644" r="182.3755" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st5" points="240.9,126.3 344.1,126.3 344.1,22.3 500.6,22.3 500.6,126.3 604.6,126.3 604.6,282 500.6,282
				500.6,386 344.1,386 344.1,282.8 240.9,282.8 "></polygon>
			<radialGradient id="SVGID_7_" cx="356.1243" cy="334.039" r="166.9874" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st6" points="189.6,262.8 284.1,262.8 284.1,167.6 427.4,167.6 427.4,262.8 522.6,262.8 522.6,405.3 427.4,405.3
				427.4,500.5 284.1,500.5 284.1,406 189.6,406 "></polygon>
			<radialGradient id="SVGID_8_" cx="385.8531" cy="291.7679" r="149.7163" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st7" points="236.6,227.9 321.3,227.9 321.3,142.6 449.7,142.6 449.7,227.9 535.1,227.9 535.1,355.6 449.7,355.6
				449.7,441 321.3,441 321.3,356.3 236.6,356.3 "></polygon>
			<radialGradient id="SVGID_9_" cx="137.2716" cy="215.8118" r="135.6454" gradientTransform="matrix(1 0 0 2 0 -29.0631)" gradientUnits="userSpaceOnUse">
				<stop  offset="0" style="stop-color:#0D79BF"></stop>
				<stop  offset="0.1015" style="stop-color:#167ABC"></stop>
				<stop  offset="0.2252" style="stop-color:#227EB6"></stop>
				<stop  offset="0.3605" style="stop-color:#3184AE"></stop>
				<stop  offset="0.5041" style="stop-color:#418DA1"></stop>
				<stop  offset="0.6544" style="stop-color:#53978E"></stop>
				<stop  offset="0.8102" style="stop-color:#66A573"></stop>
				<stop  offset="0.9677" style="stop-color:#7BB64A"></stop>
				<stop  offset="1" style="stop-color:#7FBA42"></stop>
			</radialGradient>
			<polygon class="st8" points="2.1,158 78.8,158 78.8,80.7 195.1,80.7 195.1,158 272.4,158 272.4,273.7 195.1,273.7 195.1,351
				78.8,351 78.8,274.2 2.1,274.2 "></polygon>
		</svg>
	<?php } else { ?>
		<img src="<?php echo get_template_directory_uri(); ?>/images/crosses.png"/>
	<?php }
	return ob_get_clean();
}

function the_product_ctas($ctas) {
	ob_start();
		foreach($ctas as $cta) {
			$cta = $cta['cta']; ?>
			<a class="product-cta" target="<?php echo $cta['target']; ?>" href="<?php echo $cta['url']; ?>"><?php echo $cta['title']; ?></a>
		<?php
		}
	echo ob_get_clean();
}
