( function( $ ) {

  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top + ($(window).height() / 3);
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  scrollAnimations();

  $('a[href^="#"]').click(function () {
    $('#mobile-nav').removeClass('active');
    $('html, body').animate({
        scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
    }, 500);

    return false;
  });

  $('.navbar-toggle').on('click', function () {
    console.log('ok')
    $('#mobile-nav').addClass('active');
  })

  $('#navbar-close').on('click', function () {
    $('#mobile-nav').removeClass('active');
  })

  if ( $('.statistic-container').length ) {
    $('#history-container .row').slick({
      arrows: true,
      dots: false,
      slidesToShow: 5,
      variableWidth: true,
      infinite: false,
      prevArrow: '.history-arrow-left',
      nextArrow: '.history-arrow-right',
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: true,
            slidesToShow: 1,
            variableWidth: false
          }
        }
      ]
    })
  }

  $('.product-slides').each( function() {
    const dots = $('.product-slide', this).length > 1
    $(this).on('init', function(event, slick){
      $(slick.$dots).append('<span class="slide-indicator"></span>')
    }).slick({
      arrows: false,
      dots: dots,
      customPaging : function(slider, i) {
        return '<a>'+ (i + 1) +'</a>';
      },
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
      const activeSlidePos = $('li:nth-child('+ (nextSlide + 1) +')', slick.$dots).position().left
      $('.slide-indicator', slick.$dots).css('left', activeSlidePos + 'px')
    })
  })

  $('.play-video').on('click', function(e) {
    e.preventDefault()
    const vid = document.getElementById("video-background");
    vid.play()
    $('#video-background-wrapper').addClass('active');

    $('#video-background-container').on('click', function (e) {
      if (e.target.id != 'video-background') {
        $('#video-background-wrapper').removeClass('active')
        $('#video-background-container').off('click')
        vid.pause();
      }
    })
  })

  $(window).on('resize scroll', function() {
    scrollAnimations();

    // if ( $('.statistics-container').length ) {
    //   jQuery(window).on('resize', function() {
    //     var viewportWidth = jQuery(window).width();
    //     if (viewportWidth < 481) {
    //       $('.multiple-items').slick('unslick');
    //     } else {
    //         // Do some thing
    //     }
    //   });
    // }
  });

  function scrollAnimations () {
    if( $('#about-container').isInViewport() && !$('#about-container').hasClass('active') ) {
      $('#about-container').addClass('active')
    }
    if( $('#history-container').isInViewport() ) {
      $('.statistic-number').each( function() {
        const historyNumber = $(this).data('count');
        var $this = $(this)
        $({ countNum: $this.text()}).animate({
          countNum: historyNumber
        },
        {
          duration: 4000,
          easing:'linear',
          step: function() {
            $('span',$this).html(Math.floor(this.countNum));
          },
          complete: function() {
            $('span',$this).html(this.countNum);
            //alert('finished');
          }
        });
      })
    }
    if( !$('#about-container').isInViewport() && $('#about-container').hasClass('active') ) {
      $('#about-container').removeClass('active')
    }
    $('.product-item').each(function() {
      if( $(this).isInViewport() && !$(this).hasClass('active') ) {
        $(this).addClass('active');
      }
    })
  }

} )( jQuery );
