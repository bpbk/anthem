<?php
/**
 * Theme basic setup.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'understrap_setup' );

if ( ! function_exists ( 'understrap_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function understrap_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on understrap, use a find and replace
		 * to change 'understrap' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'understrap', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'understrap' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		add_image_size( 'small', 200 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 900 );
		add_image_size( 'large', 1400 );
		add_image_size( 'full', 1900 );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'understrap_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Check and setup theme default settings.
		understrap_setup_theme_default_settings();

	}
}


add_filter( 'excerpt_more', 'understrap_custom_excerpt_more' );

if ( ! function_exists( 'understrap_custom_excerpt_more' ) ) {
	/**
	 * Removes the ... from the excerpt read more link
	 *
	 * @param string $more The excerpt.
	 *
	 * @return string
	 */
	function understrap_custom_excerpt_more( $more ) {
		if ( ! is_admin() ) {
			$more = '';
		}
		return $more;
	}
}

add_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' );

if ( ! function_exists( 'understrap_all_excerpts_get_more_link' ) ) {
	/**
	 * Adds a custom read more link to all excerpts, manually or automatically generated
	 *
	 * @param string $post_excerpt Posts's excerpt.
	 *
	 * @return string
	 */
	function understrap_all_excerpts_get_more_link( $post_excerpt ) {
		if ( ! is_admin() ) {
			$post_excerpt = $post_excerpt . ' [...]<p><a class="btn btn-secondary understrap-read-more-link" href="' . esc_url( get_permalink( get_the_ID() ) ) . '">' . __( 'Read More...',
			'understrap' ) . '</a></p>';
		}
		return $post_excerpt;
	}
}

function register_product_post_type() {
  $labels = array(
		'name'                       => _x( 'Product Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Product Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Product Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Product Types', 'iongeo' ),
		'all_items'                  => __( 'All Product Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Product Type', 'iongeo' ),
		'update_item'                => __( 'Update Product Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Product Type', 'iongeo' ),
		'new_item_name'              => __( 'New Product Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate product types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove product', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used products', 'iongeo' ),
		'not_found'                  => __( 'No products found.', 'iongeo' ),
		'menu_name'                  => __( 'Product Type', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
  	'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
  	'public'								=> false,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
    'hierarchical'          => true
	);

	register_taxonomy( 'product-type', ['product'], $args );
	register_taxonomy_for_object_type( 'product-type', ['product'] );

  $labels = [
    'name' => _x('Products', 'post type general name'),
    'singular_name' => _x('Product', 'post type singular name'),
    'add_new' => _x('Add New', 'product'),
    'add_new_item' => __('Add New Product'),
    'edit_item' => __('Edit Product'),
    'new_item' => __('New Product'),
    'view_item' => __('View Product'),
    'search_items' => __('Search Products'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Product'),
  ];
  $args = [
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'show_in_rest' => true,
    'hierarchical' => true,
    'capability_type' => 'post',
    'menu_position' => 2,
    'has_archive' => false,
    'menu_icon'   => 'dashicons-format-aside',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'],
    'taxonomies' => ['product-type'],
    'map_meta_cap' => 1,
  ];
  register_post_type( 'product' , $args );

}
add_action('init', 'register_product_post_type');
